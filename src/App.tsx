import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Navbar from './components/Navbar';
import CreateInvoice from './screens/CreateInvoice';
import DetailInvoice from './screens/DetailInvoice';
import InvoiceList from './screens/InvoiceList';

const App = () => {

  return (
    <Router>
      <Navbar/>
      <Switch>
        <Route exact path="/" component={InvoiceList} />
        <Route exact path="/create-invoice" component={CreateInvoice} />
        <Route exact path="/detail/:id" component={DetailInvoice} />
      </Switch>
    </Router>
  )
}

export default App;
