import { Link } from 'react-router-dom'

const Navbar = () => {
    return (
        <nav className="navbar navbar-dark bg-dark" >
            <div className="container-fluid">
                <Link className="navbar-brand" to="/">
                    <img src="https://static.wixstatic.com/media/63e192_877ac808025049839620feace7bbc5b5~mv2.png/v1/fill/w_464,h_232,al_c,q_85,usm_0.66_1.00_0.01/JCM%20Facturaxion.webp" alt="" width="30" height="24" className="d-inline-block align-text-top ml-2" />
                    Facturación
                </Link>
            </div>
        </nav>
    )
}

export default Navbar
