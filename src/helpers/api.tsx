import axios from 'axios'

const baseURL = `http://127.0.0.1:8000/api`;

const api = axios.create({
    baseURL,
    headers: {
        'Accept': 'application/json',
        'Content-type': 'application/json',
        'x-csrftoken': null
    }
});


export default api;