export interface Detail {
    id?: number,
    invoiceId: number,
    productId: number,
    quantity: number
}
