export interface Discount {
    id: number,
    name: string,
    value: number,
    active: boolean,
}
