export interface Invoice {
    id?: number,
    customer: string,
    date: string,
    subTotal: number,
    total: number,
    isv: number,
    discounts? : number
}
