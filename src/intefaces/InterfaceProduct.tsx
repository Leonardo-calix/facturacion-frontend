export interface Product {
    id?: number,
    name: string,
    image: string,
    price: number
    quantity: number,
    Discount_id: number,
    active: boolean
}