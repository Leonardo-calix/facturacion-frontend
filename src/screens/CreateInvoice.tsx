import { useEffect, useState } from "react"
import { createDetailInvoice, createInvoice, getDiscounts, getProducts } from "../services/services";
import { Product } from '../intefaces/InterfaceProduct';
import Select from 'react-select';
import { Discount } from "../intefaces/InterfaceDiscounts";
import moment from 'moment';
import { Link, useHistory } from "react-router-dom";
import { Invoice } from "../intefaces/InterfaceInvoice";


const CreateInvoice = () => {
    const history = useHistory();
    const [products, setProducts] = useState<Product[]>([]);
    const [optionsProducts, setoptionsProducts] = useState<any[]>([]);
    const [productsSelected, setProductsSelected] = useState<any[]>([]);
    const [discounts, setDiscounts] = useState<Discount[]>([]);
    const [total, setTotal] = useState<number>(0);
    const [subTotal, setSubTotal] = useState<number>(0);
    const [isv, setIsv] = useState<number>(0);
    const [discount, setDiscount] = useState<number>(0);
    const [error, setError] = useState<boolean>(false)
    const [customer, setCustomer] = useState<string>('');
    const [alertSuccess, setAalertSuccess] = useState<boolean>(false)
    useEffect(() => {
        getProductList();
        getDiscountList();
    }, []);

    useEffect(() => {
        getTotal();
    }, [productsSelected]);

    const createNewInvoice = async () => {

        const data: Invoice = {
            customer,
            date: moment().format('YYYY-MM-DD'),
            total,
            isv,
            subTotal,
            discounts: discount
        }


        if (customer.trim().length > 0 && productsSelected.length > 0) {
            try {
                const response = await createInvoice(data);
                if (response.status === 200) {
                    const { data } = response;
                    productsSelected.forEach(async (item: any) => {
                        await createDetailInvoice({
                            invoiceId: data.invoice,
                            productId: item.value,
                            quantity: Number(item.quantitySelectd)
                        });
                    });
                    setAalertSuccess(true);
                    setTimeout(() => {
                        history.push('/');
                    }, 1000);
                }
            } catch (error) {
                console.error(error)
            }
        } else {
            setError(true);
        }



    }


    const getProductList = async () => {
        const response = await getProducts();
        let options: any = [];

        if (response.status === 200) {
            const { data } = response;
            setProducts(data.products);

            data.products.forEach((item: Product) => {
                if (item.quantity > 0) {
                    options.push({
                        ...item,
                        value: item.id,
                        label: item.name,
                        total: 0,
                        quantitySelectd: 0,
                        discount: 0,
                    })
                }
            });
            setoptionsProducts(options);
        }
    }

    const getDiscountList = async () => {
        const response = await getDiscounts();
        if (response.status === 200) {
            const { data } = response;
            setDiscounts(data.discounts);
        }
    }

    const getDiscountById = (id: number) => {
        const discount: Discount = discounts.filter(d => d.id === id)[0];
        return { name: discount.name, value: discount.value }
    }

    const calculartotal = (quantityProducts: string, id: number, idProduct: number) => {
        const discount: number = discounts.filter(d => d.id === id)[0].value / 100;
        const price: number = products.filter(product => product.id === idProduct)[0].price;
        let discountAmount: number = (Number(quantityProducts) * price) * (discount);
        let total: number = (Number(quantityProducts) * price) - discountAmount;
        let optionsTemp = [...productsSelected];

        optionsTemp.forEach(product => {
            if (product.id == idProduct) {
                product.total = total;
                product.quantitySelectd = quantityProducts;
                product.discount = discountAmount;
            }
        });
        setProductsSelected(optionsTemp);
        getTotal();
    }

    const getTotal = () => {
        let totalTem = 0;
        let tempDiscount = 0;
        productsSelected.forEach(item => {
            totalTem += item.total
            tempDiscount += item.discount;
        });
        setTotal(totalTem);
        setIsv(totalTem * 0.15);
        setSubTotal(totalTem * 0.85);
        setDiscount(tempDiscount);
    }

    return (


        <>
            {alertSuccess &&
                <div className="d-flex justify-content-end" >
                    <div className="w-25 m-2" >
                        <div className="alert alert-success alert-dismissible fade show" role="alert">
                            Factura agregada con exito
                        </div>
                    </div>
                </div>
            }

            <div className="d-flex justify-content-center bg-light h-100" style={{ height: '100vh' }} >
                <div className="mt-2 container w-50 card p-2">
                    <h4 className="text-center m-2" >Modulo de Facturación</h4>
                    <div className="card-body">
                        <div className="mb-1 row p-2">
                            <label className="col-sm-4 col-form-label">Nombre cliente</label>
                            <div className="col-sm-8">
                                <input type="text" name="customer"
                                    value={customer}
                                    onChange={(e) => { setCustomer(e.target.value); setError(false) }}
                                    placeholder="Ingrese el nombre del cliente"
                                    className={`form-control ${error && 'is-invalid'}`} />
                            </div>
                        </div>

                        <div className="mb-1 row p-2">
                            <label className="col-sm-4 col-form-label">Fecha</label>
                            <div className="col-sm-8">
                                <input type="text" disabled value={moment().format('L')} className="form-control" />
                            </div>
                        </div>

                        <div className="mb-1 row p-2">
                            <label className="col-sm-4 col-form-label">Productos</label>
                            <div className="col-sm-8">
                                <Select
                                    isMulti
                                    name="products"
                                    options={optionsProducts}
                                    className={`basic-multi-select ${error && 'is-invalid'}`}
                                    classNamePrefix="select"
                                    onChange={(e: any) => { setProductsSelected(e); setError(false) }}
                                />
                            </div>
                        </div>
                    </div>

                    {
                        productsSelected.length > 0 &&

                        <>
                            <h4 className="text-center " >Productos seleccionado</h4>

                            <ul className="list-group">
                                <li className="list-group-item bg-dark text-white" aria-disabled="true">
                                    <div className="row">
                                        <div className="col-2">Nombre</div>
                                        <div className="col-2">Precio</div>
                                        <div className="col-2">Cantidad</div>
                                        <div className="col-4">Descuento</div>
                                        <div className="col-2">Total</div>
                                    </div>
                                </li>
                                {
                                    productsSelected.map(product => {
                                        return (
                                            <li key={product.name} className="list-group-item" aria-disabled="true">
                                                <div className="row">
                                                    <div className="col-2">{product.name}</div>
                                                    <div className="col-2">{product.price}</div>
                                                    <div className="col-2">
                                                        <input type="number" onChange={(e) => calculartotal(e.target.value, product.Discount_id, product.id)} className="form-control" />
                                                    </div>
                                                    <div className="col-4">{getDiscountById(product.Discount_id).name} </div>
                                                    <div className="col-2">{product.total}</div>
                                                </div>
                                            </li>
                                        )
                                    })
                                }
                            </ul>
                            <div className="d-flex justify-content-end" >
                                <ul className="list-group list-group-flush" style={{ width: 200 }} >
                                    <li className="list-group-item">SubTotal <b className="float-end" >{subTotal} </b></li>
                                    <li className="list-group-item">ISV <b className="float-end mr-2" >{isv} </b></li>
                                    <li className="list-group-item">Total a pagar <b className="float-end mr-2" >{total} </b> </li>
                                </ul>
                            </div>
                        </>
                        //https://react-pdf.org/?fbclid=IwAR1nzZyYuZ21104D-e5GxJbMFyBpnehhsCe-ugc8ip1b1K45uCr7QvGWYLI
                    }
                    <div className="d-flex justify-content-center" >
                        <button onClick={createNewInvoice} className="btn btn-primary btn-sm m-2" type="button">Guardar</button>
                        <Link className="btn btn-danger btn-sm m-2" to="/" role="button">Cancelar</Link>
                    </div>
                </div>
            </div>

        </>

    )
}

export default CreateInvoice
