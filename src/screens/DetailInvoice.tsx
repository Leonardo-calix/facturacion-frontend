import moment from 'moment';
import { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom'
import { Detail } from '../intefaces/InterfaceDetailInvoice';
import { Discount } from '../intefaces/InterfaceDiscounts';
import { Invoice } from '../intefaces/InterfaceInvoice';
import { Product } from '../intefaces/InterfaceProduct';
import { getDetailInvoice, getDiscounts, getInvoiceById, getProductById, getProducts } from '../services/services';

const DetailInvoice = () => {
    const params: any = useParams();
    const [details, setDetails] = useState<Detail[]>([]);
    const [productsInvoice, setProductsInvoice] = useState<any[]>([]);
    const [products, setProducts] = useState<Product[]>([]);
    const [discountsList, setDiscountslist] = useState<Discount[]>([]);
    const [loading, setLoading] = useState(true)
    const [invoice, setInvoice] = useState<Invoice>({
        customer: '',
        date: '',
        isv: 0,
        subTotal: 0,
        total: 0,
        id: 0,
        discounts: 0
    });

    const { customer, date, isv, subTotal, total, id, discounts } = invoice;

    useEffect(() => {
        getProductsList();
        getInvoice();
        getDiscountList();
    }, [loading, discountsList]);

    useEffect(() => {
        getDetail();
    }, [loading, products])

    const getInvoice = async () => {
        const response = await getInvoiceById(params.id);

        if (response.status === 200) {
            const { data } = response;
            let dataTemp: Invoice = data.invoice;

            setInvoice({
                customer: dataTemp.customer,
                total: dataTemp.total,
                subTotal: dataTemp.subTotal,
                date: dataTemp.date,
                isv: dataTemp.isv,
                id: dataTemp.id,
                discounts: dataTemp.discounts
            });
        }
        setLoading(false);
    }

    const getProductsList = async () => {
        const response = await getProducts();

        if (response.status === 200) {
            const { data } = response;
            setProducts(data.products);
        }
    }

    const getDiscountList = async () => {
        const response = await getDiscounts();
        if (response.status === 200) {
            const { data } = response;
            //console.log(data)
            setDiscountslist(data.discounts);
        }
    }

    const getDiscountById = (id: number) => {
        const discount: Discount = discountsList.filter(d => d.id === id)[0];
        if (discount) {
            return { name: discount.name, value: discount.value }
        }

        return { name: '', value: '' }

    }

    const getDetail = async () => {
        const response = await getDetailInvoice(params.id);
        let tempProductList: any[] = []

        if (response.status === 200) {
            const { data } = response;
            setDetails(data.datail);

            data.detail.forEach((item: any) => {

                let product = products.filter(p => p.id === item.Product_id)[0];
                tempProductList.push({
                    ...product,
                    quantitySelected: item.quantity
                });
            });
            setProductsInvoice(tempProductList);
        }
    }

    return (
        <div className="d-flex justify-content-center" >
            <div className="container-sm m-3 border border-info rounded-1 w-50 ">
                <div className="row p-2">
                    <div className="col-6">
                        <p className="h3 text-info" >Facturacion</p>
                        <p className="h5" >Anillo Periférico, Tegucigalpa</p>
                        <p className="h5" >Telefono : 2222-0722</p>
                        <p className="h5" >Facturacion@gmail.com</p>
                    </div>
                    <div className="col-6 d-flex justify-content-end">
                        <img width="50%" src="https://us.123rf.com/450wm/makc76/makc761709/makc76170900102/86963307-factura-facturaci%C3%B3n-facturaci%C3%B3n-servicio-en-l%C3%ADnea-usando-laptop-ilustraci%C3%B3n-vectorial.jpg?ver=6" className="img-fluid rounded-circle" alt="" />
                    </div>
                </div>

                <hr className="border border-1 border-info mt-2 mb-2" />

                <ul className="list-group list-group-flush col-md-6 bg-light m-2">
                    <li className="list-group-item bg-light">Cliente <span className="text-info" >{customer}</span></li>
                    <li className="list-group-item bg-light">Fecha <span className="text-info" >{moment(date).format('DD-MM-YYYY')}</span></li>
                    <li className="list-group-item bg-light">Fecha de expiracion <span className="text-info" >{moment(date).format('DD-MM-YYYY')}</span></li>
                    <li className="list-group-item bg-light">Condigo de factura <span className="text-info" >EA{id}</span></li>
                </ul>
                <div className="row mt-2 p-1">
                    <table className="table">
                        <thead>
                            <tr>
                                <th scope="col">Cant.</th>
                                <th scope="col">Descripcion</th>
                                <th scope="col">Descuento</th>
                                <th scope="col">Precio Unitario</th>
                                <th scope="col">Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                productsInvoice.map((product: any, key: number) => {
                                    return (
                                        <tr key={key} >
                                            <th>{product.quantitySelected}</th>
                                            <td>{product.name}</td>
                                            <td>{getDiscountById(product.Discount_id).name}</td>
                                            <td>{product.price}</td>
                                            <td>{product.quantitySelected * product.price}</td>
                                        </tr>
                                    )
                                })
                            }

                        </tbody>
                    </table>
                </div>
                <div className="row" >
                    <div className="d-flex justify-content-end p-1" >
                        <ul className="list-group list-group-flush" style={{ width: 200 }} >
                            <li className="list-group-item">SubTotal <b className="float-end" >${subTotal} </b></li>
                            <li className="list-group-item">ISV <b className="float-end mr-2" >${isv} </b></li>
                            <li className="list-group-item">Descuentos <b className="float-end" >${discounts} </b></li>
                            <li className="list-group-item">Total a pagar <b className="float-end mr-2" >${total} </b> </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default DetailInvoice;
