import { useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { Invoice } from "../intefaces/InterfaceInvoice"
import { getInvoice } from "../services/services";
import DataTable from 'react-data-table-component';

const columns = [
    {
        name: 'Cliente',
        selector: 'customer',
        sortable: true,
    },
    {
        name: 'Fecha',
        selector: 'date',
        sortable: true,
    },
    {
        name: 'Monto',
        selector: 'total',
        sortable: true,
    },
    {
        name: 'Detalle',
        selector: 'detail',
        sortable: true,
    },
];

const InvoiceList = () => {

    const [invoices, setInvoices] = useState<any[]>([]);
    const [loading, setLoading] = useState<boolean>(true)

    useEffect(() => {
        getInvoicesList();
    }, [])

    const getBtnDetail = (id: any) => {
        return (<Link to={`/detail/${id}`} className="btn btn-sm btn-success" >Detalle</Link>)
    }

    const getInvoicesList = async () => {

        let temp: any[] = [];

        const response = await getInvoice();

        if (response.status === 200) {
            const { data } = response;

            data.invoices.forEach((item: Invoice) => {
                temp.push({
                    ...item,
                    detail: getBtnDetail(item.id) || ' ',
                })
            });
            setInvoices(temp);
            setLoading(false);
        }
    }

    return (
        <div className="container w-75"  >
            <div className="d-flex justify-content-end m-2" >
                <Link className="btn btn-sm btn-warning text-white" to="/create-invoice" >agregar</Link>
            </div>

            {
                loading && <p>Cargando...</p>
            }

            {
                !loading && <DataTable
                    title="Facturas"
                    columns={columns}
                    data={invoices}
                    pagination
                    paginationRowsPerPageOptions={[5, 10, 15]}
                    paginationPerPage={5}
                />
            }

        </div >
    )
}

export default InvoiceList
