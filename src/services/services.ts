import api from '../helpers/api';
import { Detail } from '../intefaces/InterfaceDetailInvoice';
import { Invoice } from '../intefaces/InterfaceInvoice';

export const getProducts = async () => {
    try {
        const response = api.get(`/products`);
        return response;
    } catch (error) {
        return error;
    }
}

export const getProductById = async (id: number) => {
    try {
        const response = api.get(`/products/${id}`);
        return response;
    } catch (error) {
        return error;
    }
}

export const getDiscountById = async (id: number) => {
    try {
        const response = api.get(`/products${id}`);
        return response;
    } catch (error) {
        return error;
    }
}

export const getDiscounts = async () => {
    try {
        const response = api.get(`/discounts/`);
        return response;
    } catch (error) {
        return error;
    }
}

export const createInvoice = async (invoice: Invoice) => {
    try {
        const response = api.post(`/invoices/`, invoice);
        return response;
    } catch (error) {
        return error;
    }
}


export const createDetailInvoice = async (data: Detail) => {
    try {
        const response = api.post(`/details/`, data);
        return response;
    } catch (error) {
        return error;
    }
}

export const getDetailInvoice = async (invoiceId: any) => {
    try {
        const response = api.get(`/details/${invoiceId}`);
        return response;
    } catch (error) {
        return error;
    }
}

export const getInvoice = async () => {
    try {
        const response = api.get(`/invoices/`);
        return response;
    } catch (error) {
        return error;
    }
}

export const getInvoiceById = async (id: number) => {
    try {
        const response = api.get(`/invoices/${id}`);
        return response;
    } catch (error) {
        return error;
    }
}


